package com.kafkaconsumer.kafkalistner;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @KafkaListener(topics = "TOPIC_ONE")
    public void consume(String message) {
        System.out.println("Message --> "+message);
    }
}
