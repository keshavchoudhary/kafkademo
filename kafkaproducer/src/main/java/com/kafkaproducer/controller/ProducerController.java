package com.kafkaproducer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/producer")
public class ProducerController {

    @Autowired
    KafkaTemplate<String, String> template;

    public static final String KAFKA_TOPIC = "TOPIC_ONE";

    @GetMapping("/produce")
    public String produceMessage(@RequestParam String message) {
        template.send(KAFKA_TOPIC, message);
        return "MESSAGE SEND SUCCESSFULLY";
    }

}
